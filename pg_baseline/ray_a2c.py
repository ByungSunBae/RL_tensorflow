import tensorflow as tf
from tensorflow import layers

import multiprocessing as mp
import numpy as np

import os
import pickle as pick
import gym
import time
import sys

from collections import namedtuple
from collections import deque
from skimage.color import rgb2gray
from skimage.transform import resize

import ray
import random
from scipy import signal

ray.init()

C, H, W = 4, 84, 84

def get_screen(img, size=(84, 84)):
    crop_img = img[25:200, ...]
    img_gray = rgb2gray(crop_img)
    img_res = resize(img_gray, size, mode='reflect')
    return img_res[np.newaxis, ...].astype(np.float32)

def construct_network():
    x = tf.placeholder(tf.float32, [None, 84, 84, 4])
    action = tf.placeholder(tf.int32, name="action")
    adv = tf.placeholder(tf.float32, name="adv")
    dis_reward = tf.placeholder(tf.float32, name="dis_reward")
    
    with tf.variable_scope("model"):
        net = layers.conv2d(inputs=x, filters=32, kernel_size=8, strides=4, padding='valid', 
                            kernel_initializer=tf.variance_scaling_initializer, 
                            activation=tf.nn.selu, bias_initializer=tf.constant_initializer(0.0))
        
        net = layers.conv2d(inputs=net, filters=64, kernel_size=4, strides=2, padding='valid',
                     kernel_initializer=tf.variance_scaling_initializer,
                     activation=tf.nn.selu, bias_initializer=tf.constant_initializer(0.0))
        
        net = layers.conv2d(inputs=net, filters=64, kernel_size=3, strides=1, padding='valid',
                     kernel_initializer=tf.variance_scaling_initializer,
                     activation=tf.nn.selu, bias_initializer=tf.constant_initializer(0.0))
        net = layers.flatten(net)
        net = layers.dense(inputs=net, units=256, activation=tf.nn.relu, 
                           kernel_initializer=tf.variance_scaling_initializer, bias_initializer=tf.constant_initializer(0.0))
        actprob = layers.dense(inputs=net, units=2, activation=tf.nn.softmax, 
                           kernel_initializer=tf.variance_scaling_initializer, bias_initializer=tf.constant_initializer(0.0))
        crivalue = layers.dense(inputs=net, units=1, activation=None, 
                           kernel_initializer=tf.variance_scaling_initializer, bias_initializer=tf.constant_initializer(0.0))
    
    entropy = -tf.reduce_sum(actprob * tf.log(actprob + 1e-9), axis=1)
    entropy_sum = tf.reduce_sum(entropy)
    
    single_action_prob = tf.reduce_sum(actprob * tf.one_hot(action, depth=2), axis=1)
    log_action_prob = -tf.log(tf.clip_by_value(single_action_prob, 1e-9, 1.0))* adv
    actor_loss = tf.reduce_sum(log_action_prob)
    
    critic_loss = tf.reduce_sum(tf.squared_difference(dis_reward, crivalue))
    
    loss = 0.5 * critic_loss + actor_loss - 0.01 * entropy_sum
    
    optim = tf.train.AdamOptimizer(learning_rate=0.0001)
    # gvs = optim.compute_gradients(loss, tf.trainable_variables())
    gradients = tf.gradients(loss, tf.trainable_variables())
    # gradients = [grad for grad, var in gvs]
    # var_ = [var for grad, var in gvs]
    
    clipped, _ = tf.clip_by_global_norm(gradients, 40.0)
    grad_norm = tf.global_norm(clipped)
    capped_gvs = [(grad, var) for grad, var in zip(clipped, tf.trainable_variables())]
    train = optim.apply_gradients(capped_gvs)
    
    return {"pl_op": [x, action, adv, dis_reward], 
            "grad_op": capped_gvs, 
            "train_op": train,
            "loss_op": [actor_loss, critic_loss, loss],
            "actor_op": actprob, 
            "critic_op": crivalue, 
            "vars": tf.trainable_variables(),
            "entropy_term": [entropy, entropy_sum]}

# Discounting function used to calculate discounted returns.
def discount(x, gamma=0.99):
    return signal.lfilter([1], [1, -gamma], x[::-1], axis=0)[::-1]

class PongEnv(object):
    def __init__(self, use_gpu=False):
        # Tell numpy to only use one core. If we don't do this, each actor may
        # try to use all of the cores and the resulting contention may result
        # in no speedup over the serial version. Note that if numpy is using
        # OpenBLAS, then you need to set OPENBLAS_NUM_THREADS=1, and you
        # probably need to do it from the command line (so it happens before
        # numpy is imported).
        os.environ["MKL_NUM_THREADS"] = "1"
        #os.environ["CUDA_VISIBLE_DEVICES"] = "1"
        if use_gpu:
             os.environ["CUDA_VISIBLE_DEVICES"] = "0"
        else:
             os.environ["CUDA_VISIBLE_DEVICES"] = ""
        
        self.env = gym.make("Pong-v0")
        self.dict = construct_network()
        
        self.x, self.action, self.adv, self.dis_reward = self.dict["pl_op"]
        self.grad = self.dict["grad_op"]
        self.actor_loss, self.critic_loss, self.loss = self.dict["loss_op"]

        self.actprob = self.dict["actor_op"]
        self.crivalue = self.dict["critic_op"]
        self.train = self.dict["train_op"]

        self.entropy, self.entropy_sum = self.dict["entropy_term"]

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        self.init = tf.global_variables_initializer()
        self.sess.run(self.init)
        self.variables = ray.experimental.TensorFlowVariables(self.loss, self.sess)
        self.saver = tf.train.Saver(max_to_keep=10)
        
        self.t_max = 5

    def compute_gradient(self, weights):
        # Reset the game.
        _ = self.env.reset()
        # 20 frames nothing.
        _ = [self.env.step(1) for _ in range(18)]
        init_state = [get_screen(self.env.step(1)[0]) for _ in range(4)]
        
        counts = 0
        ep_reward = 0
        done = False
        
        buffer = []
        
        self.variables.set_weights(weights)
        max_probs = []
        while not done:
            states_m = []
            actions_m = []
            rewards_m = []
            next_states_m = []
            dones_m = []
            for t_ in range(self.t_max):
                if not done:
                    state = init_state if counts == 0 else state

                    act_prob = self.sess.run(self.actprob, feed_dict={self.x: np.stack(state, axis=-1)})

                    # Sample an action.
                    act = np.random.choice(np.arange(2, 4), size=1, p=act_prob[0])
                    next_frame, reward, done, info = self.env.step(act)

                    states_m.append(np.stack(state, axis=-1))
                    state.pop(0)
                    state.append(get_screen(next_frame))
                    next_states_m.append(np.stack(state, axis=-1))
                    actions_m.append(act - 2)
                    rewards_m.append(reward)
                    dones_m.append(int(done))

                    ep_reward += reward
                    counts += 1
                else: next
                
            probs_, v_ = self.sess.run([self.actprob, self.crivalue], feed_dict={self.x: np.vstack(states_m)})
            v_ = v_.reshape(-1,)
            
            max_probs.append(probs_)
            next_v_ = self.sess.run(self.crivalue, feed_dict={self.x: np.vstack(next_states_m)[-1:]}) * (1 - int(dones_m[-1]))
            next_v_ = next_v_.reshape(-1,)

            actions_ = np.hstack(actions_m)
            rewards_ = rewards_m + list(next_v_)
            dones_m = dones_m + [0]
            
            v_plus = np.array(v_.tolist() + next_v_.tolist())
            
            # dis_r = discounted_reward(rewards_, dones_m)[:-1]
            dis_r = discount(rewards_)[:-1]
            
            adv_ = np.array(rewards_m) + 0.99 * v_plus[1:] - v_plus[:-1]
            adv_ = discount(adv_, 0.99 * 0.99)
            
            self.grads_, _ = self.sess.run([self.grad, self.train], 
                                        feed_dict={self.x: np.vstack(states_m), 
                                                   self.action: actions_, 
                                                   self.adv: adv_, 
                                                   self.dis_reward: dis_r})
            
            grads_value = [v for v, k in self.grads_]
            
            buffer.append(grads_value)

        sum_grad = np.vstack(buffer).sum(axis=0)
        max_prob = np.max(np.vstack(max_probs))
        
        return sum_grad, ep_reward, max_prob
    
    def get_weights(self):
        self.params = self.variables.get_weights()
        return self.params
    
    def save_model(self, path):
        return self.saver.save(self.sess, path)
    
    def restore_model(self, path):
        return self.saver.restore(self.sess, path)

num_p = 2
remote_pong_env = ray.remote(PongEnv)
actors = [remote_pong_env.remote() for _ in range(num_p)]
local_pong_env = PongEnv(use_gpu=False)

num_episode = 10

weights = local_pong_env.get_weights()

for i_ep in range(num_episode):
    start = time.time()
    weights_id = ray.put(weights)
    actions = []
    for i in range(num_p):
        action_id = actors[i].compute_gradient.remote(weights_id)
        actions.append(action_id)
    
    ep_rewards = []
    max_probss = []
    buffers = []
    for i in range(num_p):
        action_id, actions = ray.wait(actions)
        buffer, ep_reward, max_prob = ray.get(action_id[0])
        buffers.append(buffer)
        ep_rewards.append(ep_reward)
        max_probss.append(max_prob)
    
#     if i_ep % 30 == 0:
    sum_grads = np.vstack([buffer.tolist() for buffer in buffers]).sum(axis=0).tolist()
    feed_dict = {grad[0]: sum_grad for (grad, sum_grad) in zip(local_pong_env.grad, sum_grads)}
    local_pong_env.sess.run(local_pong_env.train, feed_dict=feed_dict)
    weights = local_pong_env.get_weights()
        
    MeanScore = np.mean(ep_rewards)
    MaxScore = np.max(ep_rewards)
    MinScore = np.min(ep_rewards)
    
    MeanProb = np.round(np.mean(max_probss), 5)
    
    taken_time = np.round(time.time() - start, 3)
    print("Ep : {}\tScores : {}".format(i_ep, ep_rewards) + "\tMean Prob : {}\tTaken Time : {}".format(MeanProb, taken_time))
    #if i_ep % 100 == 0 or i_ep == num_episode - 1:
    #    local_pong_env.save_model(save_path + "-" + str(i_ep))
    #    print("Save Model : {}".format(save_path + "-" + str(i_ep)))

