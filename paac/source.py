import os
import sys

import numpy as np
import torch
import torch.nn as nn
import torch.nn.init as init
import torch.optim as optim
import torch.nn.functional as F

from skimage.color import rgb2gray
from skimage.transform import resize

class actor(nn.Module):
    def __init__(self, name):
        super(actor, self).__init__()
        self.name = name
        self.endpoints = {}
        self.conv1 = nn.Conv2d(4, 32, kernel_size=8, stride=4, bias=False)
        init.kaiming_normal_(self.conv1.weight, nonlinearity="relu")
        # init.kaiming_normal_(self.conv1.bias.reshape(-1,1), nonlinearity="relu")
        
        self.conv2 = nn.Conv2d(32, 64, kernel_size=4, stride=2, bias=False)
        init.kaiming_normal_(self.conv2.weight, nonlinearity="relu")
        # init.kaiming_normal_(self.conv2.bias.reshape(-1,1), nonlinearity="relu")
        
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3, stride=1, bias=False)
        init.kaiming_normal_(self.conv3.weight, nonlinearity="relu")
        # init.kaiming_normal_(self.conv3.bias.reshape(-1,1), nonlinearity="relu")
        
        self.head1 = nn.Linear(64 * 7 * 7, 256, bias=False)
        init.kaiming_normal_(self.head1.weight, nonlinearity="relu")
        # init.kaiming_normal_(self.head1.bias.reshape(-1,1), nonlinearity="relu")

        self.head2 = nn.Linear(256, 2, bias=False)
        init.kaiming_normal_(self.head2.weight, nonlinearity="relu")
        # init.kaiming_normal_(self.head2.bias.reshape(-1,1), nonlinearity="relu")

    def forward(self, x):
        x = F.relu(self.conv1(x))
        self.endpoints["conv1"] = x

        x = F.relu(self.conv2(x))
        self.endpoints["conv2"] = x

        x = F.relu(self.conv3(x))
        self.endpoints["conv3"] = x

        x = F.relu(self.head1(x.view(x.size(0), -1)))
        self.endpoints["head1"] = x

        x = F.softmax(self.head2(x), dim=1)
        self.endpoints["out"] = x
        return x

class critic(nn.Module):
    def __init__(self, name):
        super(critic, self).__init__()
        self.name = name
        self.endpoints = {}
        self.conv1 = nn.Conv2d(4, 32, kernel_size=8, stride=4, bias=False)
        init.kaiming_normal_(self.conv1.weight, nonlinearity="relu")
        #init.kaiming_normal_(self.conv1.bias.reshape(-1,1), nonlinearity="relu")
        
        self.conv2 = nn.Conv2d(32, 64, kernel_size=4, stride=2, bias=False)
        init.kaiming_normal_(self.conv2.weight, nonlinearity="relu")
        #init.kaiming_normal_(self.conv2.bias.reshape(-1,1), nonlinearity="relu")
        
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3, stride=1, bias=False)
        init.kaiming_normal_(self.conv3.weight, nonlinearity="relu")
        #init.kaiming_normal_(self.conv3.bias.reshape(-1,1), nonlinearity="relu")
        
        self.head1 = nn.Linear(64 * 7 * 7, 256, bias=False)
        init.kaiming_normal_(self.head1.weight, nonlinearity="relu")
        #init.kaiming_normal_(self.head1.bias.reshape(-1,1), nonlinearity="relu")

        self.head2 = nn.Linear(256, 1, bias=False)
        init.kaiming_normal_(self.head2.weight, nonlinearity="relu")
        #init.kaiming_normal_(self.head2.bias.reshape(-1,1), nonlinearity="relu")

    def forward(self, x):
        x = F.relu(self.conv1(x))
        self.endpoints["conv1"] = x

        x = F.relu(self.conv2(x))
        self.endpoints["conv2"] = x

        x = F.relu(self.conv3(x))
        self.endpoints["conv3"] = x

        x = F.relu(self.head1(x.view(x.size(0), -1)))
        self.endpoints["head1"] = x

        x = self.head2(x)
        self.endpoints["out"] = x
        return x


def discounted_reward(rewards, gamma=0.99):
    rollings = []
    rolling = 0
    for rew in reversed(rewards):
        if rew != 0:
            rolling = 0
        rolling = gamma * rolling + rew
        rollings.append(rolling)
    rollings.reverse()
    discounted_r = np.array(rollings).reshape(-1,1)
    # norm_discounted_r = (discounted_r - np.mean(discounted_r)) / (np.std(discounted_r) + 1e-7)
    return discounted_r

def discounted_reward_torch(rewards, gamma=0.99):
    rollings = []
    rolling = 0
    for rew in rewards.index_select(0, torch.linspace(len(rewards), 1, len(rewards)).long() - 1):
        if rew.detach().numpy() != 0:
            rolling = 0
        rolling = gamma * rolling + rew
        rollings.append(rolling)
    rollings.reverse()
    rollings = [rol.reshape(-1,) for rol in rollings]
    discounted_r = torch.cat(rollings).reshape(-1,1)
    # norm_discounted_r = (discounted_r - np.mean(discounted_r)) / (np.std(discounted_r) + 1e-7)
    return discounted_r

def get_screen(img, size=(84, 84)):
    img_gray = rgb2gray(img)
    img_res = resize(img_gray, size, mode='reflect')
    return img_res[np.newaxis, ...].astype(np.float32)
