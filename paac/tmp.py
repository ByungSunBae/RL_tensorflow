import os
import sys

import numpy as np
import torch
import torch.nn as nn
import torch.nn.init as init
import torch.optim as optim
import torch.nn.functional as F
import torch.multiprocessing as mp

from collections import deque
from collections import namedtuple
from skimage.color import rgb2gray
from skimage.transform import resize

import gym
from source import *

# if we can use gpu, set device cuda:0
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# device = torch.device("cpu")

print(device)

# number of environment
num_env = 16
t_max = 5

env = gym.make("Pong-v0")
gamma = 0.99
num_act = 2
C, H, W = 4, 84, 84
beta = 0.01
lr = 0.001

paac_actor = actor(name = 'actor').to(device)
paac_critic = critic(name = 'critic').to(device)

actor_optim = optim.RMSprop(paac_actor.parameters(), lr=lr, alpha=0.0224, eps=0.1, weight_decay=0.99)
critic_optim = optim.RMSprop(paac_critic.parameters(), lr=lr, alpha=0.0224, eps=0.1, weight_decay=0.99)

# For test
tmp = env.reset()
tmp_pre = get_screen(tmp)
x = torch.from_numpy(np.stack([tmp_pre, tmp_pre, tmp_pre, tmp_pre], axis=1)).to(device)

print("Actor test : {}".format(paac_actor.forward(x)))
print("Critic test : {}".format(paac_critic.forward(x)))

envs = [gym.make("Pong-v0") for _ in range(num_env)]

num_episode = 1
N_max = 115000000
N = 0
max_prob = []
for i_ep in range(num_episode):
    if N < N_max:
        _ = [env.reset() for env in envs]
        _ = [env.step(1)[0] for _ in range(20) for env in envs] # no action 20 steps
        env_init_states = [env.step(1)[0] for env in envs]
        env_probs = [[] for i in range(num_env)]
        env_state_vs = [[] for i in range(num_env)]
        env_next_state_vs = [[] for i in range(num_env)]
        env_disc_rt = [[] for i in range(num_env)]
        dones = [0 for _ in range(num_env)]
        dones_ep = [[] for _ in range(num_env)]
        counts_ = [0 for _ in range(num_env)]
        env_ep_reward = [0 for _ in range(num_env)]
        while np.all(dones) == False:
            env_states = [[] for i in range(num_env)]
            env_actions = [[] for i in range(num_env)]
            env_rewards = [[] for i in range(num_env)]
            env_next_states = [[] for i in range(num_env)]
            env_dones = [[] for i in range(num_env)]
            for env_id, env in enumerate(envs):
                states_m = []
                actions_m = []
                rewards_m = []
                next_states_m = []
                dones_m = []
                init_states = [get_screen(env_init_states[env_id], size=(84, 84)) for _ in range(4)]
                for t_ in range(t_max):
                    if dones[env_id] == 0:
                        states = init_states.copy() if counts_[env_id] == 0 else states
                        probs = paac_actor.forward(torch.from_numpy(np.stack(states, axis=1).astype(np.float32)).to(device))            
                        action = np.random.choice(np.arange(2, 4), size=1, p=np.float32(probs.cpu().data.numpy()[0, :]))[0]
                        next_frame, reward, done, _ = env.step(action)
                        states_m.append(np.vstack(states)[np.newaxis, ...])
                        states.pop(0)
                        states.append(get_screen(next_frame, size=(84, 84)))
                        next_states_m.append(np.vstack(states)[np.newaxis, ...])
                        actions_m.append(action)
                        rewards_m.append(reward)
                        env_ep_reward[env_id] += reward
                        dones_m.append(1 if reward != 0 else 0)
                        dones[env_id] = int(done)
                        dones_ep[env_id].append(int(done))
                        counts_[env_id] += 1
                    else:
                        next
                    
                st_states_m = np.vstack(states_m) if len(states_m) > 0 else np.array([], dtype=np.int32).reshape(0, C, H, W)
                st_actions_m = np.hstack(actions_m) if len(actions_m) > 0 else np.array([], dtype=np.int32).reshape(0,)
                st_rewards_m = np.hstack(rewards_m) if len(rewards_m) > 0 else np.array([], dtype=np.int32).reshape(0,)
                st_next_states_m = np.vstack(next_states_m) if len(next_states_m) > 0 else np.array([], dtype=np.int32).reshape(0, C, H, W)
                st_dones_m = np.hstack(dones_m) if len(dones_m) > 0 else np.array([], dtype=np.int32).reshape(0,)

                env_states[env_id].append(st_states_m)
                env_actions[env_id].append(st_actions_m)
                env_rewards[env_id].append(st_rewards_m)
                env_next_states[env_id].append(st_next_states_m)
                env_dones[env_id].append(st_dones_m)

            all_states = [np.vstack(env_states[env_id]).astype(np.float32) for env_id in range(num_env)] # (n_env, t_max, C, H, W)
            all_actions = [np.vstack(env_actions[env_id]).astype(np.int32) for env_id in range(num_env)] # (n_env, t_max)
            all_rewards = [np.vstack(env_rewards[env_id]).astype(np.float32) for env_id in range(num_env)] # (n_env, t_max)
            all_next_states = [np.vstack(env_next_states[env_id]).astype(np.float32) for env_id in range(num_env)] # (n_env, t_max, C, H, W)
            all_dones = [np.vstack(env_dones[env_id]).astype(np.float32) for env_id in range(num_env)] # (n_env, t_max)

            prob_list = [paac_actor.forward(torch.from_numpy(all_states[env_id]).to(device)).cpu() if len(all_states[env_id]) > 0 else [] for env_id in range(num_env)]
            v_list = [paac_critic.forward(torch.from_numpy(all_states[env_id]).to(device)).cpu() if len(all_states[env_id]) > 0 else [] for env_id in range(num_env)]
            next_v_list = [paac_critic.forward(torch.from_numpy(all_next_states[env_id][-1:, ...]).to(device)).cpu()[0] * (1 - torch.from_numpy(all_dones[env_id][:, -1])) 
            if len(all_states[env_id]) > 0 else [] for env_id in range(num_env)]

            max_prob.append(torch.max(torch.cat([prob for prob in prob_list if len(prob) > 0])).reshape(-1,1))
            if len(max_prob) > 100:
                max_prob.pop(0)
            critic_losses = [[] for _ in range(num_env)]
            actor_losses = [[] for _ in range(num_env)]
            for env_idx in range(num_env):
                if len(prob_list[env_idx]) > 0:
                    tmp_rewards = torch.cat([torch.from_numpy(all_rewards[env_idx][0]), next_v_list[env_idx]])
                    entropy = -(prob_list[env_idx] * torch.log(prob_list[env_idx])).sum(1).reshape(-1,1)
                    a = torch.from_numpy(all_actions[env_idx])
                    p_ = torch.clamp(prob_list[env_idx].gather(1, (a.long().transpose(0, 1) - 2)), min=1e-10, max=1.0)
                    dis_r = discounted_reward_torch(tmp_rewards)[:-1]
                    critic_losses[env_idx].append(torch.pow(dis_r - v_list[env_idx], 2))

                    # For preventing to put actor's gradient to critic's parameters,
                    # Copy v_list[env_idx]
                    actor_losses[env_idx].append(-torch.log(p_) * (torch.from_numpy(dis_r.detach().numpy() - v_list[env_idx].detach().numpy())) - beta * entropy)
                else:
                    next
            # break
            critic_losses = torch.cat([loss[0] for loss in critic_losses if len(loss) > 0])
            critic_cost = 0.5 * torch.mean(critic_losses).to(device)
            critic_optim.zero_grad()
            critic_cost.backward()
            critic_optim.step()

            actor_losses = torch.cat([loss[0] for loss in actor_losses if len(loss) > 0])
            actor_cost = torch.mean(actor_losses).to(device)
            actor_optim.zero_grad()
            actor_cost.backward()
            actor_optim.step()
        
        print("Episode {} - Score :".format(i_ep), env_ep_reward, "max prob : {0:.4f}".format(torch.cat(list(max_prob)).mean()))
        N += sum(counts_)
    else:
        print("The End.")
        break
