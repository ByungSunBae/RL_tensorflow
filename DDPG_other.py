# Load modules
import tensorflow as tf
from tensorflow import layers
import os

import numpy as np
import gym
from collections import deque
from collections import namedtuple

# Make environment
# env = gym.make("Pendulum-v0")
env = gym.make("MountainCarContinuous-v0")

# setting variables
act_low = env.action_space.low
act_high = env.action_space.high
state_dim = env.observation_space.shape[0]
output_dim = env.action_space.shape[0]
lr_policy = 1e-4
lr_critic = 1e-3
hiddens = [100, 80] # Two layers
memory_len = 10000
num_episodes = 100000
#num_episodes = 5
t_max = 10
gamma = 0.99
batch_size = 32
tau = 0.001

# define placeholders
state_pl = tf.placeholder(tf.float32, [None, state_dim], name="state_pl")
action_pl = tf.placeholder(tf.float32,[None, output_dim], name="action_pl")
reward_pl = tf.placeholder(tf.float32, name="reward_pl")
done_pl = tf.placeholder(tf.float32, name="done_pl")
next_state_pl = tf.placeholder(tf.float32, [None, state_dim], name="next_state_pl")
y_target_pl = tf.placeholder(tf.float32, [None, 1], name="y_target_pl")
q_grad_input = tf.placeholder(tf.float32, [None, output_dim], name="q_grad_input")

# Add placeholder operations to collection : "placeholder_ops"
_ = [tf.add_to_collection("placeholder_ops", x) for x in [state_pl, action_pl, reward_pl, next_state_pl, y_target_pl, q_grad_input]]

# define function of build_network
def critic_network(state, action, state_dim, action_dim, hiddens, scope):
    """Building critic network for ddpg"""
    #assert len(hiddens) == 2
    with tf.variable_scope(scope, dtype=tf.float32, initializer=tf.initializers.variance_scaling()):
        W1 = tf.get_variable(name="W1", shape=[state_dim, hiddens[0]])
        b1 = tf.get_variable(name="b1", shape=[hiddens[0]])
        W2 = tf.get_variable(name="W2", shape=[hiddens[0], hiddens[1]])
        W2_act = tf.get_variable(name="W2_act", shape=[action_dim, hiddens[1]])
        b2 = tf.get_variable(name="b2", shape=[hiddens[1]])
        W_out = tf.get_variable(name="W_out", shape=[hiddens[1], 1])
        b_out = tf.get_variable(name="b_out", shape=[1])
        h1 = tf.nn.relu(tf.matmul(state, W1) + b1, name="h1")
        h2 = tf.nn.relu(tf.matmul(h1, W2) + tf.matmul(action, W2_act) + b2, name="h2")
        out = tf.identity(tf.matmul(h2, W_out) + b_out, name="out")
        return out

def actor_network(state, state_dim, action_dim, hiddens, scope):
    """Building actor network for ddpg"""
    #assert len(hiddens) == 2
    with tf.variable_scope(scope, dtype=tf.float32, initializer=tf.initializers.variance_scaling()):
        W1 = tf.get_variable(name="W1", shape=[state_dim, hiddens[0]])
        b1 = tf.get_variable(name="b1", shape=[hiddens[0]])
        W2 = tf.get_variable(name="W2", shape=[hiddens[0], hiddens[1]])
        b2 = tf.get_variable(name="b2", shape=[hiddens[1]])
        W_out = tf.get_variable(name="W_out", shape=[hiddens[1], action_dim])
        b_out = tf.get_variable(name="b_out", shape=[action_dim])
        h1 = tf.nn.relu(tf.matmul(state, W1) + b1, name="h1")
        h2 = tf.nn.relu(tf.matmul(h1, W2) + b2, name="h2")
        out = tf.identity(tf.matmul(h2, W_out) + b_out, name="out")
        return tf.clip_by_value(out, act_low, act_high)


# build networks
model_policy = actor_network(state_pl, state_dim, output_dim, hiddens, scope="policy")
model_target_policy = actor_network(state_pl, state_dim, output_dim, hiddens, scope="target_policy")

model_critic = critic_network(state_pl, action_pl, state_dim, output_dim, hiddens, scope="critic")
model_target_critic = critic_network(next_state_pl, action_pl, state_dim, output_dim, hiddens, scope="target_critic")

# define Replay Memory
ReplayMemory = deque(maxlen=memory_len)

# define train operation
y_target = reward_pl + (1 - done_pl) * gamma * model_target_critic

def critic_update_func(tar_critic_val, critic_val, lr=lr_critic):
    """
    Update critic parameters.
    Args:
        tar_critic_val : tf tensor. tar_critic_val = reward + gamma * model_target_critic.
        critic_val : tf tensor. not target network, but current critic network.
        lr : float. learning rate of Adam Optimizer.
    Return:
        Operation to minimize critic loss.
    """
    critic_loss = tf.losses.mean_squared_error(tar_critic_val, critic_val)
    critic_optimizer = tf.train.AdamOptimizer(lr)
    return critic_optimizer.minimize(critic_loss)

update_critic = critic_update_func(y_target_pl, model_critic)

def actor_update_func(policy_val, grad_input, lr=lr_policy):
    """
    Update actor parameters. Not using loss of actor, but just using gradient of actor and critic.
    """
    actor_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "policy")
    actor_grads = tf.gradients(policy_val, actor_params, -grad_input)
    actor_optimizer = tf.train.AdamOptimizer(lr)
    return actor_optimizer.apply_gradients(zip(actor_grads, actor_params))

update_actor = actor_update_func(model_policy, q_grad_input)

#actor_grads = tf.gradients(model_policy, tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "policy"), -q_grad_input)
critic_grads = tf.gradients(model_critic, action_pl)

def update_networks(main_scope, target_scope, tau=0.001):
    main_net = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, main_scope)
    target_net = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, target_scope)
    #for i in range(len(main_net)):
    #    sess.run(target_net[i].assign(tau * main_net[i] + (1 - tau) * target_net[i]))
    u_ops = [target_net[i].assign(tau * main_net[i] + (1 - tau) * target_net[i]) for i in range(len(main_net))]
    return u_ops

soft_update_ops_critic = update_networks("critic", "target_critic")
soft_update_ops_policy = update_networks("policy", "target_policy")

# define session
sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)

i = 0
# Play !
for episode in range(0, num_episodes):
    ep_reward = 0
    # Initialize a random process N for action exploration (not)
    # Receive initial observation state s_1
    obs_state = env.reset()
    done = False
    #i = 0
    while done is False:
        for t_ in range(t_max):
            action = sess.run(model_policy, feed_dict={state_pl : obs_state.reshape(1,-1)}) + (np.random.normal(1)/(np.log(episode+2)))
            # if episode > 2000:
            #     env.render()
            new_state, reward, done, _ = env.step(action)
            ep_reward += reward
            # Replay Memroy order : current state, action, reward, next state
            ReplayMemory.append([obs_state.reshape(1,-1), action, reward, float(done), new_state.reshape(1,-1)])
            obs_state = new_state
            i += 1
            if t_+1 == t_max:
                sample_size = batch_size if len(ReplayMemory) > batch_size else len(ReplayMemory)
                random_idx = np.random.choice(np.arange(0, len(ReplayMemory)), size=sample_size, replace=False)
                sampled_trans = [ReplayMemory[idx] for idx in random_idx]
                curr_state_lst = np.vstack([trans[0] for trans in sampled_trans])
                action_lst = np.vstack([trans[1] for trans in sampled_trans])
                #action_lst = sess.run(action_target_op, feed_dict={state_pl : curr_state_lst})
                reward_lst = np.hstack([trans[2] for trans in sampled_trans])
                done_lst = np.hstack([trans[3] for trans in sampled_trans])
                next_state_lst = np.vstack([trans[4] for trans in sampled_trans])
                y_target_lst = sess.run(y_target, 
                                        feed_dict={next_state_pl: next_state_lst, 
                                        action_pl: sess.run(model_target_policy, feed_dict={state_pl : curr_state_lst}), 
                                        reward_pl: reward_lst.reshape(-1,1), done_pl: done_lst.reshape(-1,1)})
                _, critic_grads_val = sess.run([update_critic, critic_grads], feed_dict={state_pl: curr_state_lst, action_pl: action_lst, \
                                                                                         y_target_pl: y_target_lst})
                _ = sess.run(update_actor, feed_dict={state_pl: curr_state_lst, q_grad_input: critic_grads_val[0]})
            # if i %  C == 0:
                _ = sess.run(soft_update_ops_critic) 
                _ = sess.run(soft_update_ops_policy)
                #update_networks(sess, "critic", "target_critic", tau)
                #update_networks(sess, "policy", "target_policy", tau)
                # print("Copy ! {}".format(i))
    print("Ep : %1d | Score : %.4f" % (episode, ep_reward))
