# This file is Vanilla Policy Gradient implementation using tensorflow.

import tensorflow as tf
from tensorflow import layers

import numpy as np
import time
import sys, os
from collections import namedtuple
from collections import deque
from skimage.color import rgb2gray
from skimage.transform import resize

import gym
import random
import gc

# Named Tuple for transitions

# Breakout-v0
env = gym.make("Breakout-v0")

# History Memory ( not equal to Replay Memory. )
class HistoryMemory(object):
    """
    History Memory in Policy Gradient.
    """
    def __init__(self):
        self.memory = deque()
        self.Transition = namedtuple('Transition', ('state', 'action', 'reward'))
    def PutExperience(self, *args):
        self.memory.append(self.Transition(*args))
    def Sample(self):
        return self.memory
    def __len__(self):
        return len(self.memory)
    def reset(self):
        self.memory = deque()


# Policy Gradient
class PG(object):
    def __init__(self, env=env, gamma=0.99, 
                 state_dim=[84, 84, 4], action_dim=env.action_space.n, 
                 kernels=[1, 8, 4], filters=[1, 32, 32], strides=[1, 4, 2], 
                 learning_rate = 0.0001,
                 num_episodes = 300000,
                 allow_soft_placement=True, allow_growth=True):
        """
        Policy Gradient algorithm implementation using tensorflow.
        Args:
          env: environment of gym (openai).
          gamma: float, discounted factor in range(0, 1)
          state_dim: list, state dimension, [Height, Width, Channel]
          action_dim: int, number of actions
          kernels: list, kernel size of conv2d layers. (See BuildNetwork())
          filters: list, number of filter of conv2d layers. (See BuildNetwork())
          strides: list, number of stride of conv2d layers. (See BuildNetwork())
          learning_rate: float, learning rate of optimizer.
          num_episodes: int, number of playing and training episodes.
          allow_soft_placement: boolean, Tensorflow Session option.
          allow_growth: boolean, Tensorflow Session gpu option.
        """
        self.env = env
        self.gamma = gamma
        self.learning_rate = learning_rate
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.Height = self.state_dim[0]
        self.Width = self.state_dim[1]
        self.Channel = self.state_dim[2]
        self.kernels = kernels
        self.filters = filters
        self.strides = strides
        self.num_episodes = num_episodes
        self.allow_soft_placement=allow_soft_placement
        self.allow_growth=allow_growth
        self.scope = "policy"
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
                                                   #momentum=0.95,
                                                   #epsilon=0.01)

    def get_screen(self, screen, axis=-1):
        """
        RGB to Gray Scale & Resize game image.
        Args:
          screen: ndarray, [210, 160, 3] game image.
          axis: int, Expand Dimension of preprocessed game image.
        """
        screen = rgb2gray(screen)
        screen = np.asarray(screen, order='C')
        screen = resize(screen, (self.Height, self.Width), mode='reflect')
        return np.expand_dims(screen.astype(np.float32), axis=axis)

    def session_op(self):
        """
        Define tensorflow session operations.
        """
        config = tf.ConfigProto(allow_soft_placement=self.allow_soft_placement)
        config.gpu_options.allow_growth = self.allow_growth
        return tf.Session(config=config)

    def PlaceholderOp(self):
        """
        Define tensorflow placeholder operations.
        """
        self.state_pl = tf.placeholder(dtype=tf.float32, 
                                        shape=[None, self.Height, self.Width, self.Channel])
        self.action_pl = tf.placeholder(dtype=tf.int32, shape=[None, ])
        self.discounted_r_pl = tf.placeholder(dtype=tf.float32, shape=[None, 1])

    def BuildNetwork(self, inputs):
        """
        Build Neural Network.
        Args:
          scope: string, scope of Neural Network.
          inputs: tf.Tensor (placeholder), state_pl or next_state_pl.
        """
        with tf.variable_scope(self.scope):
            for j, kernel_, filter_, stride_ in zip(range(0, len(self.kernels)), self.kernels, self.filters, self.strides):
                if j == 0:
                    self.h = layers.conv2d(inputs=inputs, 
                                           filters=filter_, 
                                           kernel_size=kernel_,
                                           strides=stride_, 
                                           activation=tf.nn.relu,
                                           use_bias=False,
                                           kernel_initializer=tf.variance_scaling_initializer,
                                           name="h_"+str(j))
                else:
                    self.h = layers.conv2d(inputs=self.h,
                                           filters=filter_,
                                           kernel_size=kernel_,
                                           strides=stride_, 
                                           activation=tf.nn.relu,
                                           use_bias=False,
                                           kernel_initializer=tf.variance_scaling_initializer,
                                           name="h_"+str(j))
            self.h_flat = layers.flatten(self.h, name="h_flat")
            self.fc_1 = layers.dense(self.h_flat, 256, tf.nn.relu, False, 
                                     kernel_initializer=tf.variance_scaling_initializer, name="fc_1")
            self.out = layers.dense(self.fc_1, self.action_dim, tf.nn.softmax, False, 
                                    kernel_initializer=tf.variance_scaling_initializer, name="out")
        print("Build network scope : {}".format(self.scope))
        return self.out

    def reset_graph(self):
        """
        Reset Tensorflow Graphs.
        """
        return tf.reset_default_graph()

    def train_op(self):
        """
        Define train operations.
        """
        self.action_onehot = tf.one_hot(self.action_pl, depth=self.action_dim, 
                                        axis=1, on_value=1., off_value=0., dtype=tf.float32)
        self.probs = tf.reduce_sum(tf.clip_by_value(self.PolicyNet, 1e-10, 1.) * self.action_onehot, 1, True)
        self.logprobs = tf.log(self.probs)
        self.loss = tf.reduce_mean(-self.logprobs * self.discounted_r_pl)
        train_ = self.optimizer.minimize(self.loss,
            var_list=tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.scope))
        return (train_, self.loss)

    def play_train(self, memory=HistoryMemory):
        """
        Agent playing game and training.
        Args:
          memory: class. storage of history.
        """
        self.PlaceholderOp()
        self.PolicyNet = self.BuildNetwork(inputs=self.state_pl)
        self.train_and_loss = self.train_op()
        self.sess = self.session_op()
        self.init = tf.global_variables_initializer()
        self.sess.run(self.init)
        self.recent_100_reward = deque(maxlen=100)
        self.average_dq = deque(maxlen=100)
        self.memory = memory()
        for i_ep in range(self.num_episodes+1):
            self.ep_reward = 0
            state_dq = deque(maxlen=self.Channel)
            self.life_dq = deque(maxlen=2)
            _ = env.reset()
            for i in range(4):
                curr_frame, _, _, _ = env.step(0)
                state_dq.append(self.get_screen(curr_frame, axis=-1))
            self.memory.reset()
            done = False
            while done is False:
                curr_state = np.concatenate(state_dq, axis=-1)
                curr_state = np.expand_dims(curr_state, axis=0)
                self.Probs = self.sess.run(self.PolicyNet, feed_dict={self.state_pl: curr_state})
                self.average_dq.append(np.max(self.Probs))
                action = np.random.choice(a=np.arange(self.action_dim), size=1, p=self.Probs[0,:])
                if action == 0:
                    action_ = 1
                else:
                    action_ = action
                next_frame, reward, done, info = self.env.step(action_)
                self.life_dq.append(info['ale.lives'])
                if done is False:
                    if len(self.life_dq) == 2:
                        if self.life_dq[0] > self.life_dq[1]:
                            reward_t = -1.
                        else:
                            reward_t = reward
                    else:
                        reward_t = reward
                else:
                    reward_t = -1.
                next_frame = self.get_screen(next_frame, axis=-1)
                state_dq.append(next_frame)
                self.ep_reward += reward
                reward_t = np.clip(reward_t, -1., 1.)
                self.memory.PutExperience(curr_state, action, reward_t)
                if done is True:
                    transitions = self.memory.Sample()
                    self.batch = self.memory.Transition(*zip(*transitions))
                    states = np.concatenate(self.batch.state, axis=0)
                    actions = np.hstack(self.batch.action)
                    rewards = np.hstack(self.batch.reward)
                    self.rollings = []
                    rolling = 0
                    for rew in reversed(rewards):
                        if rew < 0:
                            rolling = 0
                        rolling = self.gamma * rolling + rew
                        self.rollings.append(rolling)
                    self.rollings.reverse() 
                    discounted_r = np.array(self.rollings).reshape(-1,1)
                    discounted_r = (discounted_r - np.mean(discounted_r)) / np.std(discounted_r)
                    feed_dict = {
                        self.state_pl: states,
                        self.action_pl: actions,
                        self.discounted_r_pl: discounted_r
                    }
                    self.sess.run(self.train_and_loss, feed_dict=feed_dict)
            self.recent_100_reward.append(self.ep_reward)
            if i_ep >= 10:
                action_dict = {}
                action_count = np.bincount(actions)
                for act, coun in zip(env.unwrapped.get_action_meanings(), action_count):
                    action_dict[act] = coun
                print("\n Episode %1d Done, Avg100Ep_MaxProb: %.5f, Scores: %.1f, Mean100Ep_Scores: %5f" % (i_ep,
                      np.nanmean(self.average_dq), self.ep_reward, np.mean(self.recent_100_reward)))
                print("Action matrix : %s" % action_dict)

## simple usage

# Defince Agent using PG class.
Agent = PG()

# Reset tensorflow Graph. (You don't need to execute when first define Agent.
Agent.reset_graph()

# play and train !!
Agent.play_train()
