# This file is Deep Q Network implementation using tensorflow.
# And I try to write variants of DQN like Double DQN or Dueling DQN.

import tensorflow as tf
from tensorflow import layers

import numpy as np
import time
import sys, os
from collections import namedtuple
from collections import deque
from skimage.color import rgb2gray
from skimage.transform import resize

import gym
import random

# Named Tuple for transitions

# Breakout-v0
env = gym.make("Breakout-v0")

# Replay Memory
class ReplayMemory(object):
    """
    Replay Memory in DQN.
    Args:
      max_len: int, max length of Memory.
      batch_size: int, number of batch size that samples from Memory.
    """
    def __init__(self, max_len=100000, batch_size=32):
        self.max_len = max_len
        self.memory = deque(maxlen = self.max_len)
        self.batch_size = batch_size
        self.Transition = namedtuple('Transition', ('state', 'action', 'next_state', 'reward', 'done'))
    def PutExperience(self, *args):
        self.memory.append(self.Transition(*args))
    def Sample(self):
        return random.sample(self.memory, self.batch_size)
    def __len__(self):
        return len(self.memory)


# Deep Q Network
class DQN(object):
    def __init__(self, env=env, gamma=0.99, 
                 state_dim=[84, 84, 4], action_dim=env.action_space.n, 
                 kernels=[8, 4, 3], filters=[32, 64, 64], strides=[4, 2, 1], 
                 learning_rate = 0.00025,
                 C = 10000,
                 num_episodes = 50000,
                 memory=ReplayMemory, max_len=200000, batch_size=32,
                 allow_soft_placement=True, allow_growth=True):
        """
        DQN algorithm implementation using tensorflow.
        Args:
          env: environment of gym (openai).
          gamma: float, discounted factor in range(0, 1)
          state_dim: list, state dimension, [Height, Width, Channel]
          action_dim: int, number of actions
          kernels: list, kernel size of conv2d layers. (See BuildNetwork())
          filters: list, number of filter of conv2d layers. (See BuildNetwork())
          strides: list, number of stride of conv2d layers. (See BuildNetwork())
          learning_rate: float, learning rate of optimizer.
          C: int, Cycle of Copy Main Network variable to Target Network's.
          num_episodes: int, number of playing and training episodes.
          memory: class, Replay Memory.
          max_len: int, max length of Memory.
          batch_size: int, number of sampling batch size from Replay Memory.
          allow_soft_placement: boolean, Tensorflow Session option.
          allow_growth: boolean, Tensorflow Session gpu option.
        """
        self.env = env
        self.gamma = gamma
        self.learning_rate = learning_rate
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.Height = self.state_dim[0]
        self.Width = self.state_dim[1]
        self.Channel = self.state_dim[2]
        self.kernels = kernels
        self.filters = filters
        self.strides = strides
        self.C = C
        self.num_episodes = num_episodes
        self.max_len = max_len
        self.batch_size = batch_size
        self.memory = memory(max_len=self.max_len, batch_size=self.batch_size)
        self.allow_soft_placement=allow_soft_placement
        self.allow_growth=allow_growth
        self.main_scope = "main"
        self.target_scope = "target"
    
    def get_screen(self, screen, axis=-1):
        """
        RGB to Gray Scale & Resize game image.
        Args:
          screen: ndarray, [210, 160, 3] game image.
          axis: int, Expand Dimension of preprocessed game image.
        """
        screen = rgb2gray(screen)
        screen = np.asarray(screen, order='C')
        screen = resize(screen, (self.Height, self.Width), mode='reflect')
        return np.expand_dims(screen.astype(np.float32), axis=axis)

    def session_op(self):
        """
        Define tensorflow session operations.
        """
        config = tf.ConfigProto(allow_soft_placement=self.allow_soft_placement)
        config.gpu_options.allow_growth = self.allow_growth
        return tf.Session(config=config)

    def PlaceholderOp(self):
        """
        Define tensorflow placeholder operations.
        """
        self.state_pl = tf.placeholder(dtype=tf.float32, 
                                        shape=[None, self.Height, self.Width, self.Channel])
        self.action_pl = tf.placeholder(dtype=tf.int32)
        self.reward_pl = tf.placeholder(dtype=tf.float32, shape=[None, 1])
        self.done_pl = tf.placeholder(dtype=tf.float32, shape=[None, 1])
        self.next_state_pl = tf.placeholder(dtype=tf.float32,
                                             shape=[None, self.Height, self.Width, self.Channel])

    def BuildNetwork(self, scope, inputs):
        """
        Build Neural Network.
        Args:
          scope: string, scope of Neural Network.
          inputs: tf.Tensor (placeholder), state_pl or next_state_pl.
        """
        self.scope = scope
        with tf.variable_scope(self.scope):
            for j, kernel_, filter_, stride_ in zip(range(0, len(self.kernels)), self.kernels, self.filters, self.strides):
                if j == 0:
                    self.h = layers.conv2d(inputs=inputs, 
                                           filters=filter_, 
                                           kernel_size=kernel_,
                                           strides=stride_, 
                                           activation=tf.nn.selu,
                                           use_bias=False,
                                           name="h_"+str(j))
                else:
                    self.h = layers.conv2d(inputs=self.h,
                                           filters=filter_,
                                           kernel_size=kernel_,
                                           strides=stride_, 
                                           activation=tf.nn.selu,
                                           use_bias=False,
                                           name="h_"+str(j))
            self.h_flat = layers.flatten(self.h, name="h_flat")
            self.fc_1 = layers.dense(self.h_flat, 512, tf.nn.selu, False, name="fc_1")
            self.out = layers.dense(self.fc_1, self.action_dim, None, False, name="out")
        print("Build network scope : {}".format(self.scope))
        return self.out

    def reset_graph(self):
        """
        Reset Tensorflow Graphs.
        """
        return tf.reset_default_graph()

    def train_op(self):
        """
        Define train operations.
        """
        self.action_onehot = tf.one_hot(self.action_pl, depth=self.action_dim, 
                                        axis=1, on_value=1., off_value=0., dtype=tf.float32)
        self.Qvalues = self.MainNet * self.action_onehot
        self.nextQvalues = tf.reduce_max(self.TargetNet, 1, True)
        self.targetQvalues = self.reward_pl + self.gamma * (1. - self.done_pl) * self.nextQvalues
        loss = tf.reduce_mean(tf.losses.huber_loss(labels=self.targetQvalues, predictions=self.Qvalues))
        optimizer = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate,
                                              momentum=0.95,
                                              epsilon=0.01)
        train_ = optimizer.minimize(loss, 
            var_list=tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.main_scope))
        return (train_, loss)

    def copy_params(self):
        """
        Copy Main Network variables to Target Network's
        """
        main_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.main_scope)
        target_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.target_scope)
        for i in range(len(main_)):
            self.sess.run(target_[i].assign(main_[i]))

    def play_train(self):
        """
        Agent playing game and training.
        """
        self.PlaceholderOp()
        self.MainNet = self.BuildNetwork(scope=self.main_scope, inputs=self.state_pl)
        self.TargetNet = self.BuildNetwork(scope=self.target_scope, inputs=self.next_state_pl)
        self.train_and_loss = self.train_op()
        self.sess = self.session_op()
        self.init = tf.global_variables_initializer()
        self.sess.run(self.init)
        
        # copy variables to target network
        self.copy_params()
        self.frame = 0
        self.steps_done = 0
        self.eps_start = 1.
        self.eps_end = .1
        self.eps_decay = 1e6
        self.recent_100_reward = deque(maxlen=100)
        self.average_dq = deque(maxlen=100)
        for i_ep in range(self.num_episodes+1):
            self.actions = deque()
            self.ep_reward = 0
            state_dq = deque(maxlen=self.Channel)
            life_dq = deque(maxlen=2)
            _ = env.reset()
            for i in range(4):
                curr_frame, _, _, _ = env.step(0)
                state_dq.append(self.get_screen(curr_frame, axis=-1))
            done = False
            while done is False:
                self.frame += 1
                self.curr_state = np.concatenate(state_dq, axis=-1)
                self.curr_state = np.expand_dims(self.curr_state, axis=0)
                samp = random.random()
                Qvalues = self.sess.run(self.MainNet, feed_dict={self.state_pl: self.curr_state})
                MaxQ = np.max(Qvalues)
                self.average_dq.append(MaxQ)
                # Epsilon Greedy
                if self.frame >= 50000:
                    self.steps_done += 1
                    self.eps_threshold = self.eps_start + min(float(self.steps_done)/self.eps_decay, 1.) * (self.eps_end - self.eps_start)
                else:
                    self.eps_threshold = self.eps_start

                if samp > self.eps_threshold:
                    action = np.argmax(Qvalues, 1)
                else:
                    action = random.randrange(self.env.action_space.n)
                self.actions.append(action)
                next_frame, reward, done, info = self.env.step(action)
                life_dq.append(info['ale.lives'])
                if done is False:
                    if len(life_dq) == 2:
                        if life_dq[0] > life_dq[1]:
                            done_t = 1.
                            reward_t = -1
                        else:
                            done_t = 0.
                            reward_t = reward
                    else:
                        done_t = float(done)
                        reward_t = reward
                else:
                    done_t = float(done)
                    reward_t = -1
                next_frame = self.get_screen(next_frame, axis=-1)
                state_dq.append(next_frame)
                self.next_state = np.concatenate(state_dq, axis=-1)
                self.next_state = np.expand_dims(self.next_state, axis=0)
                self.ep_reward += reward
                reward_t = np.clip(reward_t, -1., 1.)
                self.memory.PutExperience(self.curr_state, action, self.next_state, reward_t, done_t)
                if self.frame > 50000:
                    if len(self.memory.memory) < self.batch_size:
                        next
                    else:
                        transitions = self.memory.Sample()
                        self.batch = self.memory.Transition(*zip(*transitions))
                        final_mask = np.vstack(self.batch.done)
                        state_batch = np.concatenate(self.batch.state, axis=0)
                        action_batch = np.hstack(self.batch.action)
                        next_state_batch = np.concatenate(self.batch.next_state, axis=0)
                        reward_batch = np.vstack(self.batch.reward)
                        feed_dict = {
                            self.state_pl: state_batch,
                            self.action_pl: action_batch,
                            self.reward_pl: reward_batch,
                            self.done_pl: final_mask,
                            self.next_state_pl: next_state_batch
                        }
                        self.sess.run(self.train_and_loss, feed_dict=feed_dict)
                        if self.frame % self.C == 0:
                            self.copy_params()
                            print("Copy Main to Target network !")
            self.recent_100_reward.append(self.ep_reward)
            if i_ep >= 10:
                action_dict = {}
                action_count = np.bincount(np.hstack(self.actions))
                for act, coun in zip(env.unwrapped.get_action_meanings(), action_count):
                    action_dict[act] = coun
                print("\n Episode %1d Done, Frames: %1d, E-Greedy: %.5f, Scores: %.1f, Mean100Ep_Scores: %5f, AvgMaxQ: %.5f" % (i_ep,
                      self.frame, self.eps_threshold, self.ep_reward, np.mean(self.recent_100_reward), np.nanmean(self.average_dq)))
                print("Action matrix : %s" % action_dict)

## simple usage

# Defince Agent using DQN class.
Agent = DQN()

# Reset tensorflow Graph. (You don't need to execute when first define Agent.
Agent.reset_graph()

# play and train !!
Agent.play_train()
